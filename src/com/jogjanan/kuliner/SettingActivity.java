package com.jogjanan.kuliner;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class SettingActivity extends Activity implements OnItemClickListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		ActionBar ab = getActionBar();
		ab.setTitle("Setting");
		ab.setDisplayHomeAsUpEnabled(true);

		String[] settings = { "Account", "Preference" };

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, settings);
		ListView lv = (ListView) findViewById(R.id.listSetting);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			break;

		default:
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		if (arg0.getItemAtPosition(arg2).toString().equalsIgnoreCase("account")) {
			startActivity(new Intent(this, ProfileAccountActivity.class));
		} else {
			startActivity(new Intent(this, ProfilePreferenceActivity.class));
		}
	}
}
