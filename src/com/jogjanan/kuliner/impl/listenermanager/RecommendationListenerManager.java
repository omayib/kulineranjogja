package com.jogjanan.kuliner.impl.listenermanager;

import com.jogjanan.kuliner.model.listener.OnRecommendationAddedListener;

public class RecommendationListenerManager {
	private OnRecommendationAddedListener mListeners = null;

	public void setRecommendationListener(OnRecommendationAddedListener listener) {
		mListeners = listener;
	}

	public OnRecommendationAddedListener getListeners() {
		return mListeners;
	}

}
