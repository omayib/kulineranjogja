package com.jogjanan.kuliner.impl.listenermanager;

import com.jogjanan.kuliner.model.listener.OnGcmRegisteredListener;


public class GCMListenerManager {
	private OnGcmRegisteredListener mListeners = null;

	public void setOnGcmRegisteredListener(OnGcmRegisteredListener listener) {
		mListeners = listener;
	}

	public OnGcmRegisteredListener getAllListeners() {
		return mListeners;
	}

}