package com.jogjanan.kuliner.impl.listenermanager;

import com.jogjanan.kuliner.model.listener.OnNearbyAddedListener;

public class NearbyListenerManager {
	private OnNearbyAddedListener mListeners = null;

	public void setNearbyListener(OnNearbyAddedListener listener) {
		mListeners = listener;
	}

	public OnNearbyAddedListener getListeners() {
		return mListeners;
	}

}
