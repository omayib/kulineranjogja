package com.jogjanan.kuliner.impl.listenermanager;

import com.jogjanan.kuliner.model.listener.OnAccountChangedListener;

public class AccountChangedListenerManager {
	private OnAccountChangedListener mListeners = null;

	public void setAccountSavedListener(OnAccountChangedListener listener) {
		mListeners = listener;
	}

	public OnAccountChangedListener getListeners() {
		return mListeners;
	}

}
