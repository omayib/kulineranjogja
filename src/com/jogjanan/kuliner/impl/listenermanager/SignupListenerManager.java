package com.jogjanan.kuliner.impl.listenermanager;

import com.jogjanan.kuliner.model.listener.OnRegisteredListener;

public class SignupListenerManager {
	private OnRegisteredListener mListeners = null;

	public void setSignupListener(OnRegisteredListener listener) {
		mListeners = listener;
	}

	public OnRegisteredListener getListeners() {
		return mListeners;
	}

}
