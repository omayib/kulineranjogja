package com.jogjanan.kuliner.impl.listenermanager;

import com.jogjanan.kuliner.model.listener.OnPreferenceActionListener;

public class PreferenceListenerManager {
	private OnPreferenceActionListener mListeners = null;

	public void setOnPreferenceLoadedListener(
			OnPreferenceActionListener listener) {
		mListeners = listener;
	}

	public OnPreferenceActionListener getListeners() {
		return mListeners;
	}

}
