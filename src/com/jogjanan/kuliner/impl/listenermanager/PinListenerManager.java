package com.jogjanan.kuliner.impl.listenermanager;

import com.jogjanan.kuliner.model.listener.OnPinnedListener;

public class PinListenerManager {
	private OnPinnedListener mListeners = null;

	public void setPinListener(OnPinnedListener listener) {
		mListeners = listener;
	}

	public OnPinnedListener getListeners() {
		return mListeners;
	}

}
