package com.jogjanan.kuliner.impl.listenermanager;

import com.jogjanan.kuliner.model.listener.OnLoginListener;
import com.jogjanan.kuliner.model.listener.OnLogoutListener;
import com.jogjanan.kuliner.model.listener.OnSignupListener;

public class AuthListenerManager {
	private OnLoginListener mLoginListeners = null;
	private OnSignupListener mSignupListener = null;
	private OnLogoutListener mLogoutListeners = null;

	public void setLogoutListener(OnLogoutListener listener) {
		mLogoutListeners = listener;
	}

	public OnLogoutListener getOnLogoutListener() {
		return mLogoutListeners;
	}

	public void setOnLoginListener(OnLoginListener listener) {
		mLoginListeners = listener;
	}

	public OnLoginListener getOnLoginListeners() {
		return mLoginListeners;
	}

	public void setOnSignupListener(OnSignupListener listener) {
		mSignupListener = listener;
	}

	public OnSignupListener getOnSignupListener() {
		return mSignupListener;
	}
}
