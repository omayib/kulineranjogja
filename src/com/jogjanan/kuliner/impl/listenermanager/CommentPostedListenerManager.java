package com.jogjanan.kuliner.impl.listenermanager;

import com.jogjanan.kuliner.model.listener.OnCommentPostedListener;


public class CommentPostedListenerManager {
	private OnCommentPostedListener mListeners = null;

	public void setCommentPostedListener(OnCommentPostedListener listener) {
		mListeners = listener;
	}

	public OnCommentPostedListener getAllListeners() {
		return mListeners;
	}

}
