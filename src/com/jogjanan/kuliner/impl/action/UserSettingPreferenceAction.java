package com.jogjanan.kuliner.impl.action;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.jogjanan.kuliner.impl.client.GetPreferenceAsync;
import com.jogjanan.kuliner.impl.client.UpdatePreferenceAsync;
import com.jogjanan.kuliner.impl.listenermanager.PreferenceListenerManager;
import com.jogjanan.kuliner.model.ItemPreference;
import com.jogjanan.kuliner.model.StatusAction;

public class UserSettingPreferenceAction {
	private PreferenceListenerManager preferenceLoadedListener;

	public UserSettingPreferenceAction(PreferenceListenerManager listener) {
		super();
		this.preferenceLoadedListener = listener;
	}

	public void loadPreference(Context c, String token) {
		GetPreferenceAsync pref = new GetPreferenceAsync(c) {

			@Override
			public void ResponValue(boolean status, String respon) {
				// TODO Auto-generated method stub
				if (!status) {
					preferenceLoadedListener.getListeners().onPreferenceLoaded(
							null, StatusAction.FAILED);
					return;
				}
				try {
					JSONObject jo = new JSONObject(respon);
					JSONObject pref = jo.getJSONObject("preference");

					ItemPreference item = new ItemPreference(
							pref.getLong("manis"), pref.getLong("asam"),
							pref.getLong("asin"), pref.getLong("pahit"),
							pref.getLong("gurih"), pref.getLong("pedas"),
							pref.getLong("panas"), pref.getLong("dingin"));

					preferenceLoadedListener.getListeners().onPreferenceLoaded(
							item, StatusAction.SUCCESS);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		};
		pref.execute(token);
	}

	public void savePreference(Context c, String token, ItemPreference pref) {
		UpdatePreferenceAsync updatePref = new UpdatePreferenceAsync(c) {

			@Override
			public void ResponValue(boolean status) {
				// TODO Auto-generated method stub
				if (!status) {

					preferenceLoadedListener.getListeners().onPreferenceSaved(
							StatusAction.FAILED);
				} else {

					preferenceLoadedListener.getListeners().onPreferenceSaved(
							StatusAction.SUCCESS);
				}
			}
		};
		updatePref.execute(pref.getManis(), pref.getAsam(), pref.getAsin(),
				pref.getPahit(), pref.getGurih(), pref.getPedas(),
				pref.getPanas(), pref.getDingin(), token);
	}
}