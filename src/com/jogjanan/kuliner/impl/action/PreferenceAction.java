package com.jogjanan.kuliner.impl.action;

import com.jogjanan.kuliner.helper.PreferencesHelper;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferenceAction {
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor preferenceEditor;

	public PreferenceAction(SharedPreferences sharedPreferences,
			Editor preferenceEditor) {
		this.sharedPreferences = sharedPreferences;
		this.preferenceEditor = preferenceEditor;
	}

	public void setToken(String token) {
		preferenceEditor.putString(PreferencesHelper.TOKEN, token);
		preferenceEditor.commit();
	}

	public void setUsername(String username) {
		preferenceEditor.putString(PreferencesHelper.USERNAME, username);
		preferenceEditor.commit();
	}

	public String getToken() {
		return sharedPreferences.getString(PreferencesHelper.TOKEN, "");
	}

	public void clearPreferences() {
		preferenceEditor.clear();
		preferenceEditor.commit();
	}

	public String getGCMRegistrationId() {
		return sharedPreferences.getString(PreferencesHelper.PROPERTY_REG_ID,
				"");
	}

	public int getRegisteredVersion() {
		return sharedPreferences.getInt(PreferencesHelper.PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
	}

	public void storeGCMRegistrationId(String value, int appVersion) {
		preferenceEditor.putString(PreferencesHelper.PROPERTY_REG_ID, value);
		preferenceEditor.putInt(PreferencesHelper.PROPERTY_APP_VERSION,
				appVersion);
		preferenceEditor.commit();

	}

	public void setNewUser(boolean b) {
		// TODO Auto-generated method stub
		preferenceEditor.putBoolean(PreferencesHelper.NEWUSER, b);
		preferenceEditor.commit();
	}

	public boolean isNewUser() {
		return sharedPreferences.getBoolean(PreferencesHelper.NEWUSER, false);
	}

	public void setPhotoPath(String selectedImagePath) {
		// TODO Auto-generated method stub
		preferenceEditor.putString(PreferencesHelper.PHOTO_PATH, "");
		preferenceEditor.commit();

	}

}
