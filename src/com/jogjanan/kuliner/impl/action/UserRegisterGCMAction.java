package com.jogjanan.kuliner.impl.action;

import android.content.Context;

import com.jogjanan.kuliner.impl.client.GCMRegisterAsync;
import com.jogjanan.kuliner.impl.listenermanager.GCMListenerManager;
import com.jogjanan.kuliner.model.StatusAction;

public class UserRegisterGCMAction {
	private GCMListenerManager mListener;

	public UserRegisterGCMAction(GCMListenerManager listener) {

		this.mListener = listener;
	}

	public void doRegister(Context ct, String senderID) {

		GCMRegisterAsync register = new GCMRegisterAsync(ct) {

			@Override
			public void ResponValue(boolean status, String respon) {
				// TODO Auto-generated method stub

				if (!status) {
					mListener.getAllListeners().onGCMRegistered(
							StatusAction.FAILED, null);
					System.out.println("failed");
				} else {
					mListener.getAllListeners().onGCMRegistered(
							StatusAction.SUCCESS, respon);
					System.out.println(respon);
				}
			}
		};
		register.execute(senderID);
	}

}