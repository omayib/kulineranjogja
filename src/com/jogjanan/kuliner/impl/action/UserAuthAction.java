package com.jogjanan.kuliner.impl.action;

import android.content.Context;

import com.jogjanan.kuliner.impl.client.LoginAsync;
import com.jogjanan.kuliner.impl.client.LogoutAsync;
import com.jogjanan.kuliner.impl.client.SignupAsync;
import com.jogjanan.kuliner.impl.listenermanager.AuthListenerManager;
import com.jogjanan.kuliner.model.User;

public class UserAuthAction {
	private AuthListenerManager mListener;

	public UserAuthAction(AuthListenerManager listener) {
		super();
		this.mListener = listener;
	}

	public void doLogin(Context ct, final String email, final String password,
			String regID) {
		LoginAsync login = new LoginAsync(ct) {

			@Override
			public void ResponValue(boolean status, String respon) {
				// TODO Auto-generated method stub
				if (!status) {
					mListener.getOnLoginListeners().onLogin(null, false);// result
				} else {
					mListener.getOnLoginListeners().onLogin(
							new User("Arif akbarul", respon, 12, email,
									password, ""), true);// result
				}
			}
		};
		login.execute(email, password, regID);

	}

	public void doSignup(Context ct, final String email, final String password,
			String regID) {
		SignupAsync signup = new SignupAsync(ct) {

			@Override
			public void ResponValue(boolean status, String token) {
				// TODO Auto-generated method stub
				if (!status) {

					mListener.getOnSignupListener().onSignup(null, false);
				} else {
					mListener.getOnSignupListener().onSignup(
							new User("Arif akbarul", token, 12, email,
									password, ""), true);
				}
			}
		};
		signup.execute(email, password, regID);
	}

	public void doLogout(Context ct, String token) {
		LogoutAsync logout = new LogoutAsync(ct) {

			@Override
			public void ResponValue(boolean status) {
				// TODO Auto-generated method stub
				mListener.getOnLogoutListener().onLogout(status);
			}
		};
		logout.execute(token);
	}
}
