package com.jogjanan.kuliner.impl.action;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;

import com.jogjanan.kuliner.impl.client.GetAccountAsync;
import com.jogjanan.kuliner.impl.client.ImageDownloadTaks;
import com.jogjanan.kuliner.impl.client.SettingAccountAsync;
import com.jogjanan.kuliner.impl.listenermanager.AccountChangedListenerManager;
import com.jogjanan.kuliner.model.StatusAction;
import com.jogjanan.kuliner.model.User;

public class UserSettingAccountAction {
	private AccountChangedListenerManager mListener;

	public UserSettingAccountAction(AccountChangedListenerManager listener) {
		super();
		this.mListener = listener;
	}

	public void doSave(Context ct, User user) {
		SettingAccountAsync account = new SettingAccountAsync(ct) {

			@Override
			public void ResponValue(boolean status) {
				// TODO Auto-generated method stub
				System.out.println(status);
				if (!status) {
					mListener.getListeners().onAccountSaved(false);
				} else {
					mListener.getListeners().onAccountSaved(true);
				}
			}

		};
		account.execute(user.getEmail(), user.getPassword(), user.getName(),
				user.getToken(), user.getPhoto());

	}

	public void getAccountFromApi(Context ct, final String token) {
		GetAccountAsync getAccount = new GetAccountAsync(ct) {

			@Override
			public void ResponValue(boolean status, String respon) {
				// TODO Auto-generated method stub
				JSONObject ob;
				try {
					ob = new JSONObject(respon);
					JSONObject jUser = ob.getJSONObject("profile");
					User user = new User(jUser.getString("fullName"), token,
							jUser.getInt("ID"), jUser.getString("email"),
							jUser.getString("password"),
							jUser.getString("photo"));

					mListener.getListeners().onAccountLoaded(
							StatusAction.SUCCESS, user);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

					mListener.getListeners().onAccountLoaded(
							StatusAction.FAILED, null);
				}

			}
		};
		getAccount.execute(token);
	}

	public void loadPicture(String photo) {
		// TODO Auto-generated method stub
		ImageDownloadTaks img = new ImageDownloadTaks() {

			@Override
			public void ResponValue(Bitmap bitmap) {
				// TODO Auto-generated method stub
				if (bitmap != null) {
					mListener.getListeners().onPictureLoaded(
							StatusAction.SUCCESS, bitmap);

				} else {
					mListener.getListeners().onPictureLoaded(
							StatusAction.FAILED, null);
				}
			}
		};
		img.execute(photo);
	}

}
