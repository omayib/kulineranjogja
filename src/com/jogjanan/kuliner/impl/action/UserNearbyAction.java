package com.jogjanan.kuliner.impl.action;

import com.jogjanan.kuliner.impl.DataGenerator;
import com.jogjanan.kuliner.impl.listenermanager.NearbyListenerManager;
import com.jogjanan.kuliner.model.StatusAction;

public class UserNearbyAction {
	private NearbyListenerManager mListener;

	public UserNearbyAction(NearbyListenerManager listener) {
		super();
		this.mListener = listener;
	}

	public void exploreNearby(float lat, float lng) {
		// client to explore
		DataGenerator data = new DataGenerator(null);

		mListener.getListeners().onNearbyAdded(data.getRestaurants(),
				StatusAction.SUCCESS);
	}

}
