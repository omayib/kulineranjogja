package com.jogjanan.kuliner.impl.action;

import java.util.ArrayList;

import com.jogjanan.kuliner.impl.listenermanager.RecommendationListenerManager;
import com.jogjanan.kuliner.model.ItemRecommendation;
import com.jogjanan.kuliner.model.StatusAction;
import com.jogjanan.kuliner.model.listener.OnRecommendationAddedListener;

public class UserRecommendationAction {
	private RecommendationListenerManager mListener;

	public UserRecommendationAction(RecommendationListenerManager listener) {
		super();
		this.mListener = listener;
	}

	public void getRecommendation(String token, int idUser) {
		// class untuk menapatan recommendation

		if (token.equalsIgnoreCase("123")) {
			ArrayList<ItemRecommendation> listReco = new ArrayList<ItemRecommendation>();

			ItemRecommendation itemReco = new ItemRecommendation("soto",
					"soto kuus", "jalan", "url", 2, 3, 1);
			listReco.add(itemReco);
			mListener.getListeners().onRecommendationAdded(listReco,
					StatusAction.SUCCESS);
		} else {
			mListener.getListeners().onRecommendationAdded(null,
					StatusAction.FAILED);
		}
	}

}
