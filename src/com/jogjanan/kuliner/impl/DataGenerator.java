package com.jogjanan.kuliner.impl;

import java.util.ArrayList;
import java.util.Random;

import com.jogjanan.kuliner.model.Comment;
import com.jogjanan.kuliner.model.ItemMenuRestoran;
import com.jogjanan.kuliner.model.ItemRecommendation;
import com.jogjanan.kuliner.model.Restaurant;
import com.jogjanan.kuliner.model.Taste;
import com.jogjanan.kuliner.sqlite.DataBaseHelper;

public class DataGenerator {
	private ArrayList<Comment> arrComments = new ArrayList<Comment>();
	private Comment comment;

	private ArrayList<ItemMenuRestoran> arrMenus = new ArrayList<ItemMenuRestoran>();;
	private DataBaseHelper dbHelper;

	public DataGenerator(DataBaseHelper dataBaseHelper) {
		// TODO Auto-generated constructor stub
		this.dbHelper = dataBaseHelper;
	}

	public ArrayList<ItemMenuRestoran> getMenuRestaurant(int idresto) {
		return dbHelper.getMenuResto(idresto);
	}

	public Restaurant getDetailResto(int idLocalResto) {
		return dbHelper.getDetailResto(idLocalResto);
	}

	public void generateData() {
		ArrayList<Restaurant> restaurants = generateRestaurant();
		fillDatabase(restaurants);
	}

	public void fillDatabase(ArrayList<Restaurant> arrResto) {
		dbHelper.insertResto(arrResto);
	}

	public ArrayList<ItemRecommendation> getRecommendation() {

		return dbHelper.getRecommendation();

	}

	public ArrayList<Restaurant> getRestaurants() {
		return null;
	}

	private ArrayList<Comment> generateComments() {
		comment = new Comment("arif akbarul", "02/03/2014",
				"wah enak tenan #ayamGeprek disini! jossss", 0, 1, 0);
		arrComments.add(comment);
		comment = new Comment("ridhani", "02/03/2014",
				"manteb #ayamGeprek disini! jossss", 0, 1, 1);
		arrComments.add(comment);

		comment = new Comment("hammam", "03/03/2014",
				"puedesss #ayamGeprek disini! jossss", 0, 1, 1);
		arrComments.add(comment);

		comment = new Comment("bagus H", "03/03/2014",
				"WOwWW! #ayamGeprek disini! jossss", 0, 1, 0);
		arrComments.add(comment);

		comment = new Comment("Evan dimas", "04/03/2014",
				"wluar biasa.. #ayamGeprek disini! jossss", 0, 1, 2);
		arrComments.add(comment);

		return arrComments;

	}

	private ArrayList<ItemMenuRestoran> generateMenus() {
		ItemMenuRestoran menu1 = new ItemMenuRestoran("ayam bakar", 12000,
				"http://photo.com/aa.png", 5, true, 1, 0, generateTaste());
		ItemMenuRestoran menu2 = new ItemMenuRestoran("ayam krispi", 19000,
				"http://photo.com/aa.png", 4, false, 1, 1, generateTaste());
		ItemMenuRestoran menu3 = new ItemMenuRestoran("ayam geprek", 8000,
				"http://photo.com/aa.png", 1, false, 1, 2, generateTaste());
		ItemMenuRestoran menu4 = new ItemMenuRestoran("sate kere", 2000,
				"http://photo.com/aa.png", 4, false, 1, 3, generateTaste());
		ItemMenuRestoran menu5 = new ItemMenuRestoran("nasi kucing", 2000,
				"http://photo.com/aa.png", 2, false, 1, 4, generateTaste());
		arrMenus.add(menu1);
		arrMenus.add(menu2);
		arrMenus.add(menu3);
		arrMenus.add(menu4);
		arrMenus.add(menu5);
		return arrMenus;

	}

	private ArrayList<Taste> generateTaste() {
		Random random = new Random();
		int randomLevel = random.nextInt(5);
		ArrayList<Taste> arrTaste1 = new ArrayList<Taste>();
		Taste taste1 = new Taste("gurih", randomLevel);
		Taste taste2 = new Taste("manis", randomLevel);
		Taste taste3 = new Taste("asam", randomLevel);
		Taste taste4 = new Taste("asin", randomLevel);
		Taste taste5 = new Taste("pedas", randomLevel);
		arrTaste1.add(taste1);
		arrTaste1.add(taste2);
		arrTaste1.add(taste3);
		arrTaste1.add(taste4);
		arrTaste1.add(taste5);
		return arrTaste1;
	}

	public ArrayList<Restaurant> generateRestaurant() {
		ArrayList<Restaurant> arrResto = new ArrayList<Restaurant>();
		Restaurant resto;

		resto = new Restaurant("Spesial Sambal", "jalan jalan", 1, "outdor", 1,
				13, generateMenus(), generateComments(), -71234, 1101231);

		arrResto.add(resto);
		return arrResto;

	}

	public ArrayList<Comment> getCommentResto(int idLocalResto) {
		return dbHelper.getCommentResto(idLocalResto);
		// TODO Auto-generated method stub

	}
}
