package com.jogjanan.kuliner.impl.client;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.jogjanan.kuliner.helper.URLapi;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class GetPreferenceAsync extends
		AsyncTask<Object, Object, Object> {
	ProgressDialog pd;
	Context ct;

	public abstract void ResponValue(boolean status, String respon);

	public GetPreferenceAsync(Context c) {
		this.ct = c;
	}

	@Override
	protected void onPostExecute(Object result) {
		// TODO Auto-generated method stub
		if (pd.isShowing()) {
			pd.dismiss();
		}
		String res = (String) result;
		try {
			JSONObject obj = new JSONObject(res);
			if (!obj.getString("item").isEmpty()) {
				ResponValue(true, res);
			} else {
				ResponValue(false, null);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ResponValue(false, null);
		}
		super.onPostExecute(result);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub

		pd = new ProgressDialog(ct);
		pd.setMessage("load...");
		pd.show();

		super.onPreExecute();
	}

	@Override
	protected Object doInBackground(Object... params) {
		// TODO Auto-generated method stub
		String data = null;
		try {
			data = ClientToServer.HttpGet(URLapi.getItemPreference);
			System.out.println("Data::" + data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return data;
	}

}
