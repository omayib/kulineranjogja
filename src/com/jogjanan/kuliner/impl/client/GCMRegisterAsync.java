package com.jogjanan.kuliner.impl.client;

import java.io.IOException;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class GCMRegisterAsync extends
		AsyncTask<Object, Object, Object> {
	ProgressDialog pd;
	Context ct;

	public abstract void ResponValue(boolean status, String respon);

	public GCMRegisterAsync(Context c) {
		this.ct = c;
	}

	@Override
	protected void onPostExecute(Object result) {
		// TODO Auto-generated method stub
		if (pd.isShowing()) {
			pd.dismiss();
		}
		if (result != null) {
			String res = (String) result;
			System.out.println("registered" + res);
			ResponValue(true, res);
		} else {
			ResponValue(false, null);
		}

		super.onPostExecute(result);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub

		pd = new ProgressDialog(ct);
		pd.setTitle("Registering");
		pd.setMessage("registering push notification...");
		pd.show();

		super.onPreExecute();
	}

	@Override
	protected Object doInBackground(Object... params) {
		// TODO Auto-generated method stub
		String msg = "";
		String regid = null;
		try {
			GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(ct);
			regid = gcm.register(params[0].toString());
			msg = "Device registered, registration ID=" + regid;
		} catch (IOException ex) {
			msg = "Error :" + ex.getMessage();
		}
		return regid;
	}
}