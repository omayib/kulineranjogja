package com.jogjanan.kuliner.impl.client;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.jogjanan.kuliner.helper.URLapi;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class SettingAccountAsync extends
		AsyncTask<Object, Object, Object> {
	ProgressDialog pd;
	Context ct;

	public abstract void ResponValue(boolean status);

	public SettingAccountAsync(Context c) {
		this.ct = c;
	}

	@Override
	protected void onPostExecute(Object result) {
		// TODO Auto-generated method stub
		if (pd.isShowing()) {
			pd.dismiss();
		}
		if (result == null) {
			return;
		}
		String res = (String) result;
		try {
			JSONObject obj = new JSONObject(res);
			if (!obj.getString("status").equalsIgnoreCase("success")) {
				ResponValue(false);
			} else {
				ResponValue(true);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ResponValue(false);
		}
		super.onPostExecute(result);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub

		pd = new ProgressDialog(ct);
		pd.setMessage("Login...");
		pd.show();

		super.onPreExecute();
	}

	@Override
	protected Object doInBackground(Object... params) {
		// TODO Auto-generated method stub
		String data = null;
		ArrayList<NameValuePair> kirimkephp = new ArrayList<NameValuePair>();
		kirimkephp.add(new BasicNameValuePair("email", params[0].toString()));
		kirimkephp
				.add(new BasicNameValuePair("password", params[1].toString()));
		kirimkephp
				.add(new BasicNameValuePair("fullName", params[2].toString()));
		kirimkephp.add(new BasicNameValuePair("token", params[3].toString()));
		NameValuePair picParam = new BasicNameValuePair("pictures",
				params[4].toString());
		try {
			data = ClientToServer.fileUpload(URLapi.updateProfile, kirimkephp,
					picParam);
			System.out.println("Data::" + data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return data;
	}

}
