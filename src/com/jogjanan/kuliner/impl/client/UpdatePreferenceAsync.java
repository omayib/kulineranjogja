package com.jogjanan.kuliner.impl.client;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.jogjanan.kuliner.helper.URLapi;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class UpdatePreferenceAsync extends
		AsyncTask<Object, Object, Object> {
	ProgressDialog pd;
	Context ct;

	public abstract void ResponValue(boolean status);

	public UpdatePreferenceAsync(Context c) {
		this.ct = c;
	}

	@Override
	protected void onPostExecute(Object result) {
		// TODO Auto-generated method stub
		if (pd.isShowing()) {
			pd.dismiss();
		}
		String res = (String) result;
		try {
			JSONObject obj = new JSONObject(res);
			if (!obj.getString("status").equalsIgnoreCase("success")) {
				ResponValue(false);
			} else {
				ResponValue(true);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ResponValue(false);
		}
		super.onPostExecute(result);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub

		pd = new ProgressDialog(ct);
		pd.setMessage("Login...");
		pd.show();

		super.onPreExecute();
	}

	@Override
	protected Object doInBackground(Object... params) {
		// TODO Auto-generated method stub
		String data = null;
		ArrayList<NameValuePair> kirimkephp = new ArrayList<NameValuePair>();
		kirimkephp.add(new BasicNameValuePair("manis", params[0].toString()));
		kirimkephp.add(new BasicNameValuePair("asam", params[1].toString()));
		kirimkephp.add(new BasicNameValuePair("asin", params[2].toString()));
		kirimkephp.add(new BasicNameValuePair("pahit", params[3].toString()));
		kirimkephp.add(new BasicNameValuePair("gurih", params[4].toString()));
		kirimkephp.add(new BasicNameValuePair("pedas", params[5].toString()));
		kirimkephp.add(new BasicNameValuePair("panas", params[6].toString()));
		kirimkephp.add(new BasicNameValuePair("dingin", params[7].toString()));
		kirimkephp.add(new BasicNameValuePair("token", params[8].toString()));

		try {
			data = ClientToServer.HttpPost(URLapi.updatePreference, kirimkephp);
			System.out.println("Data::" + data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return data;
	}

}
