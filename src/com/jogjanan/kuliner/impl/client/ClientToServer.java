package com.jogjanan.kuliner.impl.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class ClientToServer {
	public static final int HTTP_TIMEOUT = 30 * 1000;
	private static HttpClient client;
	static FileInputStream fileInputStream = null;
	static String sResponse;

	private static HttpClient getHttpClient() {
		if (client == null) {
			client = new DefaultHttpClient();
			final HttpParams parameterHttp = client.getParams();

			HttpConnectionParams.setConnectionTimeout(parameterHttp,
					HTTP_TIMEOUT);
			ConnManagerParams.setTimeout(parameterHttp, HTTP_TIMEOUT);
		}
		return client;

	}

	public static String HttpPost(String url,
			ArrayList<NameValuePair> postParameters) throws Exception {
		Log.d("url", url);
		BufferedReader in = null;
		try {
			HttpClient client = getHttpClient();
			HttpPost req = new HttpPost(url);
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(
					postParameters);
			req.setEntity(formEntity);
			HttpResponse respon = client.execute(req);
			in = new BufferedReader(new InputStreamReader(respon.getEntity()
					.getContent()));

			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();
			String hasil = sb.toString();
			return hasil;
		} finally {
			if (in != null) {
				in.close();
			}

		}

	}

	public static String HttpGet(String url) throws Exception {
		Log.d("omayib", url);
		BufferedReader in = null;
		try {
			HttpClient hc = getHttpClient();
			HttpGet req = new HttpGet();
			req.setURI(new URI(url));
			HttpResponse resp = hc.execute(req);
			in = new BufferedReader(new InputStreamReader(resp.getEntity()
					.getContent()));

			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();
			String hasil = sb.toString();
			return hasil;
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public static String fileUpload(String url,
			ArrayList<NameValuePair> postParameters, NameValuePair picParameters) {
		try {
			String picParamName = picParameters.getName();
			String filepath = picParameters.getValue();
			File f = new File(filepath);
			FileBody fbin = new FileBody(f);

			HttpClient client = getHttpClient();
			HttpPost req = new HttpPost(url);
			HttpContext localContext = new BasicHttpContext();

			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			// byte[] data = bos.toByteArray();

			for (NameValuePair value : postParameters) {
				entity.addPart(value.getName(),
						new StringBody(value.getValue()));
			}

			// entity.addPart("namamenu", new StringBody(postParameters.get(0)
			// .getValue()));
			// entity.addPart("deskripsi", new StringBody(postParameters.get(1)
			// .getValue()));
			// entity.addPart("harga", new StringBody(postParameters.get(2)
			// .getValue()));
			// entity.addPart("idjenismenu", new
			// StringBody(postParameters.get(3)
			// .getValue()));
			// entity.addPart("idwarung", new StringBody(postParameters.get(4)
			// .getValue()));

			entity.addPart(picParamName, fbin);

			req.setEntity(entity);
			// req.setEntity(formEntity);
			HttpResponse response = client.execute(req, localContext);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(), "UTF-8"));

			sResponse = reader.readLine();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sResponse;

	}

	public static boolean isNetworkAvailable(Context con) {
		ConnectivityManager connectivityManager = (ConnectivityManager) con
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}
