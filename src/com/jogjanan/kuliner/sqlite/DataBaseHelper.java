package com.jogjanan.kuliner.sqlite;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jogjanan.kuliner.model.Comment;
import com.jogjanan.kuliner.model.ItemMenuRestoran;
import com.jogjanan.kuliner.model.ItemRecommendation;
import com.jogjanan.kuliner.model.Restaurant;
import com.jogjanan.kuliner.model.Taste;

public class DataBaseHelper extends SQLiteOpenHelper {
	long lastid = -1;
	// The Android's default system path of your application database.
	@SuppressLint("SdCardPath")
	private static String DB_PATH = "/data/data/com.jogjanan.kuliner/databases/";

	private static String DB_NAME = "kuliner.sqlite";
	private SQLiteDatabase myDataBase;
	private final Context myContext;
	// tables name
	private static final String TABLE_RESTO = "resto";
	private static final String TABLE_COMMENT = "comment";
	private static final String TABLE_MENU = "menu";
	// table resto row
	private static final String RESTO_IDLOCAL = "idlocal";
	private static final String RESTO_IDRESTO = "idresto";
	private static final String RESTO_NAME = "name";
	private static final String RESTO_DISTANCE = "distance";
	private static final String RESTO_ADDR = "address";
	private static final String RESTO_LAT = "lat";
	private static final String RESTO_LNG = "lng";
	private static final String RESTO_AREA = "area";
	private static final String RESTO_PINNED = "pinned";

	// table comment row
	private static final String COMMENT_IDLOCAL = "idlocal";
	private static final String COMMENT_IDCOMMENT = "idcomment";
	private static final String COMMENT_NAME = "name";
	private static final String COMMENT_DATE = "date";
	private static final String COMMENT_MESSAGE = "message";
	private static final String COMMENT_IDRESTO = "idresto";
	private static final String COMMENT_IDMENU = "idmenu";
	// table menu row
	private static final String MENU_IDLOCAL = "idlocal";
	private static final String MENU_IDMENU = "idmenu";
	private static final String MENU_NAME = "name";
	private static final String MENU_PRICE = "price";
	private static final String MENU_RATE = "rate";
	private static final String MENU_RECOMMENDED = "recommended";
	private static final String MENU_IDRESTO = "idresto";
	private static final String MENU_PICTURE = "picture";
	private static final String MENU_TASTE = "taste";

	/**
	 * Constructor Takes and keeps a reference of the passed context in order to
	 * access to the application assets and resources.
	 * 
	 * @param context
	 */
	public DataBaseHelper(Context context) {

		super(context, DB_NAME, null, 2);
		this.myContext = context;

	}

	/**
	 * Creates a empty database on the system and rewrites it with your own
	 * database.
	 * */
	public void createDataBase() throws IOException {

		boolean dbExist = checkDataBase();

		if (dbExist) {
			Log.d("database", "db Exist");
		} else {

			this.getReadableDatabase();
			this.close();
			try {
				copyDataBase();
				// DeleteOldDatabase();
			} catch (IOException e) {

				throw new Error("Error copying database");

			}
		}

	}

	private void DeleteOldDatabase() {
		// TODO Auto-generated method stub
		Log.d("database", "DeleteOldDatabase");
		if (checkOldDataBase()) {
			myContext.deleteDatabase(DB_NAME);
			Log.d("database", "Deleting old Database");
		}

	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 
	 * @return true if it exists, false if it doesn't
	 */
	private boolean checkDataBase() {

		SQLiteDatabase checkDB = null;

		try {

			String myPath = DB_PATH + DB_NAME;
			Log.d("database", myPath);
			checkDB = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READONLY);
			Log.d("checkdb", myContext.getAssets().toString());
		} catch (SQLiteException e) {
			Log.d("checkdb", "tidak ada");
			// database does't exist yet.
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (checkDB != null) {
			Log.d("checkdb", "close");
			checkDB.close();

		}

		return checkDB != null ? true : false;
	}

	private boolean checkOldDataBase() {

		SQLiteDatabase checkDB = null;

		try {

			String myPath = DB_PATH + DB_NAME;
			checkDB = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READONLY);
			Log.d("checkdb", myContext.getAssets().toString());
		} catch (SQLiteException e) {
			Log.d("checkdb", "tidak ada");
			// database does't exist yet.
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (checkDB != null) {
			Log.d("checkdb", "close");
			checkDB.close();

		}

		return checkDB != null ? true : false;
	}

	/**
	 * Copies your database from your local assets-folder to the just created
	 * empty database in the system folder, from where it can be accessed and
	 * handled. This is done by transfering bytestream.
	 * */
	private void copyDataBase() throws IOException {

		// Open your local db as the input stream
		InputStream myInput = myContext.getAssets().open(DB_NAME);

		// Path to the just created empty db
		String outFileName = DB_PATH + DB_NAME;
		myDataBase = myContext.openOrCreateDatabase(DB_PATH + DB_NAME,
				Context.MODE_PRIVATE, null);

		// Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);

		// transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}

		// Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();

	}

	public boolean isOpenDB() {

		return myDataBase != null && myDataBase.isOpen();
	}

	public void openDataBase() throws SQLException {

		// Open the database
		String myPath = DB_PATH + DB_NAME;
		myDataBase = SQLiteDatabase.openDatabase(myPath, null,
				SQLiteDatabase.OPEN_READWRITE);

	}

	@Override
	public synchronized void close() {

		if (myDataBase != null)
			myDataBase.close();

		super.close();

	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.d("database local", String.valueOf(oldVersion));
		Log.d("database local", String.valueOf(newVersion));

	}

	public void insertResto(ArrayList<Restaurant> rastaurants) {
		for (Restaurant resto : rastaurants) {
			ContentValues cval = new ContentValues();

			cval.put(RESTO_ADDR, resto.getAddress());
			cval.put(RESTO_AREA, resto.getArea());
			cval.put(RESTO_DISTANCE, resto.getDistance());
			cval.put(RESTO_IDRESTO, resto.getIdRestaurant());
			cval.put(RESTO_LAT, resto.getLatitude());
			cval.put(RESTO_LNG, resto.getLongitude());
			cval.put(RESTO_NAME, resto.getName());

			myDataBase.insert(TABLE_RESTO, null, cval);

			// isnert menu
			for (ItemMenuRestoran menu : resto.getMenus()) {
				ContentValues valmenu = new ContentValues();

				valmenu.put(MENU_IDMENU, menu.getIdMenuRestaurant());
				valmenu.put(MENU_IDRESTO, menu.getIdRestaurant());
				valmenu.put(MENU_NAME, menu.getName());
				valmenu.put(MENU_PRICE, menu.getHarga());
				valmenu.put(MENU_RATE, menu.getRate());
				valmenu.put(MENU_RECOMMENDED, menu.isRecommended());
				valmenu.put(MENU_PICTURE, menu.getPhoto());
				valmenu.put(MENU_TASTE, menu.getTasteAsString());

				myDataBase.insert(TABLE_MENU, null, valmenu);
			}

			for (Comment comment : resto.getComments()) {
				ContentValues valcomment = new ContentValues();
				valcomment.put(COMMENT_DATE, comment.getDate());
				valcomment.put(COMMENT_IDCOMMENT, comment.getIdComment());
				valcomment.put(COMMENT_IDMENU, comment.getIdMenu());
				valcomment.put(COMMENT_IDRESTO, comment.getIdResto());
				valcomment.put(COMMENT_MESSAGE, comment.getMessage());
				valcomment.put(COMMENT_NAME, comment.getName());

				myDataBase.insert(TABLE_COMMENT, null, valcomment);

			}
		}
	}

	public ArrayList<ItemRecommendation> getRecommendation() {
		System.out.println("executed");
		ArrayList<ItemRecommendation> dataRecommendation = new ArrayList<ItemRecommendation>();
		Cursor cur;
		ItemRecommendation itemrecommended;
		try {
			/*
			 * SELECT m.name,m.idmenu,m.idresto,r.name,r.address FROM menu m
			 * JOIN resto r ON m.idresto=r.idresto WHERE m.recommended=1
			 */
			cur = myDataBase
					.rawQuery(
							"SELECT m.name,m.idmenu,m.idresto,r.name,r.address,r.idlocal "
									+ "FROM menu m JOIN resto r ON m.idresto=r.idresto "
									+ "WHERE m.recommended=1", null);

			cur.moveToFirst();
			if (!cur.isAfterLast()) {
				do {
					itemrecommended = new ItemRecommendation(cur.getString(0),
							cur.getString(3), cur.getString(4),
							"cursor for phoro", cur.getInt(2), cur.getInt(1),
							cur.getInt(5));
					System.out.println("executed");
					dataRecommendation.add(itemrecommended);

				} while (cur.moveToNext());

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("DEBE ERROR", e.toString());

		}
		return dataRecommendation;

	}

	public ArrayList<ItemMenuRestoran> getMenuResto(int idResto) {
		System.out.println("executed");
		ArrayList<ItemMenuRestoran> menuResto = new ArrayList<ItemMenuRestoran>();
		Cursor cur;
		ItemMenuRestoran menu;
		try {
			/*
			 * idlocal,idmenu,name,price,rate,recommended,idresto,taste,pic
			 */
			cur = myDataBase.rawQuery("SELECT * FROM menu WHERE idresto="
					+ idResto, null);
			cur.moveToFirst();
			if (!cur.isAfterLast()) {
				do {
					/*
					 * String name, double harga, String photo, float rate,
					 * boolean recommended, int idRestoran, int idMenuRestoran,
					 * ArrayList<Taste> tastes
					 */
					menu = new ItemMenuRestoran(cur.getString(2),
							cur.getDouble(3), cur.getString(8),
							cur.getFloat(4), true, cur.getInt(6),
							cur.getInt(1), cur.getString(7));

					menuResto.add(menu);

				} while (cur.moveToNext());

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("DEBE ERROR", e.toString());

		}
		return menuResto;

	}

	public void setPin(int idLocal) {
		ContentValues val = new ContentValues();
		val.put(RESTO_PINNED, true);
		myDataBase.update(TABLE_MENU, val, RESTO_IDLOCAL + "=" + idLocal, null);
	}

	public void unPin(int idLocal) {
		ContentValues val = new ContentValues();
		val.put(RESTO_PINNED, false);
		myDataBase.update(TABLE_MENU, val, RESTO_IDLOCAL + "=" + idLocal, null);
	}

	public void clearDatabase() {
		// TODO Auto-generated method stub
		myDataBase.delete(TABLE_COMMENT, null, null);
		myDataBase.delete(TABLE_MENU, null, null);
		myDataBase.delete(TABLE_RESTO, null, null);
		
	}

	public Restaurant getDetailResto(int idLocalResto) {
		// TODO Auto-generated method stub
		Cursor cur;
		Restaurant resto = null;
		try {
			/*
			 * idlocal,idresto,name,distance,address,lat,lng,area,pinned,
			 * totalcomment
			 */
			cur = myDataBase.rawQuery(
					"SELECT r.*, count(c.name)  as totalComment  FROM resto r JOIN comment c ON "
							+ "r.idresto=c.idresto WHERE r.idlocal="
							+ idLocalResto, null);
			cur.moveToFirst();
			resto = new Restaurant(cur.getString(2), cur.getString(4),
					cur.getString(7), cur.getInt(1), cur.getInt(9),
					cur.getFloat(5), cur.getFloat(6), cur.getFloat(3));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("DEBE ERROR", e.toString());

		}

		return resto;
	}

	public ArrayList<Comment> getCommentResto(int idLocalResto) {
		// TODO Auto-generated method stub
		Cursor cur;
		ArrayList<Comment> listComment = new ArrayList<Comment>();

		try {
			/*
			 * idlocal,idcomment,name,data,message,idresto,idmenu
			 */
			cur = myDataBase.rawQuery("SELECT * FROM comment WHERE idResto="
					+ idLocalResto, null);
			cur.moveToFirst();

			if (!cur.isAfterLast()) {
				do {
					System.out.println(cur.getString(3));
					Comment comment = new Comment(cur.getString(2),
							cur.getString(3), cur.getString(4), cur.getInt(1),
							cur.getInt(5), cur.getInt(6));

					listComment.add(comment);

				} while (cur.moveToNext());

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("DEBE ERROR", e.toString());

		}
		return listComment;
	}
}
