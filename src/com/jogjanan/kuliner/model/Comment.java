package com.jogjanan.kuliner.model;

public class Comment {
	private String name;
	private String date;
	private String message;
	private int idcomment, idresto, idmenu;

	public Comment(String name, String date, String message, int idcomment,
			int idresto, int idmenu) {
		super();
		this.name = name;
		this.date = date;
		this.message = message;
		this.idcomment = idcomment;
		this.idresto = idresto;
		this.idmenu = idmenu;
	}

	public String getName() {
		return name;
	}

	public String getDate() {
		return date;
	}

	public String getMessage() {
		return message;
	}

	public int getIdComment() {
		return idcomment;
	}

	public int getIdResto() {
		return idresto;
	}

	public int getIdMenu() {
		return idmenu;
	}
}
