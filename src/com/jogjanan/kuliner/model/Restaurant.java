package com.jogjanan.kuliner.model;

import java.util.ArrayList;

public class Restaurant {
	private String name, address, area;
	private int idRestoran, totalComment;
	private ArrayList<ItemMenuRestoran> menus;
	private ArrayList<Comment> comments;
	private float lat, lng, distance;

	/**
	 * insert data restaurant
	 * 
	 * @param someValue
	 *            A description of what this parameter is for. Should include 8
	 *            * restriction, if any, on the values passed in. 9 * 10
	 * @param anotherValue
	 *            A description of what this parameter is for. Should include 11
	 *            * restriction, if any, on the values passed in. 12 * 13 *
	 * @return A description of what value(s) are returned and under what
	 *         specific 14 * circumstances. 15 * 16 *
	 * @throws InvalidArgumentException
	 *             describe when this exception is thrown by the 17 * method. 18
	 */
	public Restaurant(String name, String address, float distance, String area,
			int idRestoran, int totalComment,
			ArrayList<ItemMenuRestoran> menus, ArrayList<Comment> comments,
			float lat, float lng) {
		super();
		this.name = name;
		this.address = address;
		this.distance = distance;
		this.area = area;
		this.idRestoran = idRestoran;
		this.totalComment = totalComment;
		this.menus = menus;
		this.comments = comments;
		this.lat = lat;
		this.lng = lng;
	}

	public Restaurant(String name, String address, String area, int idRestoran,
			int totalComment, float lat, float lng, float distance) {
		super();
		this.name = name;
		this.address = address;
		this.area = area;
		this.idRestoran = idRestoran;
		this.totalComment = totalComment;
		this.lat = lat;
		this.lng = lng;
		this.distance = distance;
	}

	public float getLongitude() {
		return lng;
	}

	public float getLatitude() {
		return lat;
	}

	public ArrayList<Comment> getComments() {
		return comments;
	}

	public ArrayList<ItemMenuRestoran> getMenus() {
		return menus;
	}

	public int getCommentTotal() {
		return totalComment;
	}

	public int getIdRestaurant() {
		return idRestoran;
	}

	public String getArea() {
		return area;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public float getDistance() {
		return distance;
	}
}
