package com.jogjanan.kuliner.model;

public class ItemPreference {
	private float manis, asam, asin, pahit, gurih, pedas, panas, dingin;

	public ItemPreference(float manis, float asam, float asin, float pahit,
			float gurih, float pedas, float panas, float dingin) {
		super();
		this.manis = manis;
		this.asam = asam;
		this.asin = asin;
		this.pahit = pahit;
		this.gurih = gurih;
		this.pedas = pedas;
		this.panas = panas;
		this.dingin = dingin;
	}

	public float getManis() {
		return manis;
	}

	public float getAsam() {
		return asam;
	}

	public float getAsin() {
		return asin;
	}

	public float getPahit() {
		return pahit;
	}

	public float getGurih() {
		return gurih;
	}

	public float getPedas() {
		return pedas;
	}

	public float getPanas() {
		return panas;
	}

	public float getDingin() {
		return dingin;
	}

}
