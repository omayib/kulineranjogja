package com.jogjanan.kuliner.model;

public class ItemRecommendation {
	private String nameFood, nameResto, addressResto, photoResto;
	private int idResto, idMenu, idRestoLocal;

	public ItemRecommendation(String nameFood, String nameResto,
			String addressResto, String photoResto, int idResto, int idMenu,
			int idRestoLocal) {
		super();
		this.nameFood = nameFood;
		this.nameResto = nameResto;
		this.addressResto = addressResto;
		this.photoResto = photoResto;
		this.idResto = idResto;
		this.idMenu = idMenu;
		this.idRestoLocal = idRestoLocal;
	}

	public int getIdRestoLocal() {
		return idRestoLocal;
	}

	public void setIdRestoLocal(int idRestoLocal) {
		this.idRestoLocal = idRestoLocal;
	}

	public String getNameFood() {
		return nameFood;
	}

	public String getNameResto() {
		return nameResto;
	}

	public String getAddressResto() {
		return addressResto;
	}

	public String getPhotoResto() {
		return photoResto;
	}

	public int getIdResto() {
		return idResto;
	}

	public int getIdMenu() {
		return idMenu;
	}

	public void setNameFood(String nameFood) {
		this.nameFood = nameFood;
	}

	public void setNameResto(String nameResto) {
		this.nameResto = nameResto;
	}

	public void setAddressResto(String addressResto) {
		this.addressResto = addressResto;
	}

	public void setPhotoResto(String photoResto) {
		this.photoResto = photoResto;
	}

	public void setIdResto(int idResto) {
		this.idResto = idResto;
	}

	public void setIdMenu(int idMenu) {
		this.idMenu = idMenu;
	}

}
