package com.jogjanan.kuliner.model;

public class Taste {
	private String taste;
	private float level;

	public Taste(String taste, float level) {
		super();
		this.taste = taste;
		this.level = level;
	}

	public String getTaste() {
		return taste;
	}

	public float getLevel() {
		return level;
	}
}
