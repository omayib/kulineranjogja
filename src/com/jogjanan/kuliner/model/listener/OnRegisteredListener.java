package com.jogjanan.kuliner.model.listener;

import com.jogjanan.kuliner.model.User;

public interface OnRegisteredListener {

	void onRegistered(User user, boolean status);

}
