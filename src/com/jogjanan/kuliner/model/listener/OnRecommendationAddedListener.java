package com.jogjanan.kuliner.model.listener;

import java.util.ArrayList;

import com.jogjanan.kuliner.model.ItemRecommendation;
import com.jogjanan.kuliner.model.StatusAction;

public interface OnRecommendationAddedListener {
	void onRecommendationAdded(
			ArrayList<ItemRecommendation> itemRecommendation, StatusAction status);

}
