package com.jogjanan.kuliner.model.listener;

import com.jogjanan.kuliner.model.User;

public interface OnLoginListener {

	void onLogin(User user, Boolean status);

}
