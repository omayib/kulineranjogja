package com.jogjanan.kuliner.model.listener;

import com.jogjanan.kuliner.model.Comment;

public interface OnCommentPostedListener {

	void onCommentPosted(Comment comment, boolean status);

}
