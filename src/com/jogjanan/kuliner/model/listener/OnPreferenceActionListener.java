package com.jogjanan.kuliner.model.listener;

import com.jogjanan.kuliner.model.ItemPreference;
import com.jogjanan.kuliner.model.StatusAction;

public interface OnPreferenceActionListener {
	void onPreferenceLoaded(ItemPreference preferences, StatusAction status);

	void onPreferenceSaved(StatusAction status);
}
