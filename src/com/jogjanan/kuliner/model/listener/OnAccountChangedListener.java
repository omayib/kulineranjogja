package com.jogjanan.kuliner.model.listener;

import android.graphics.Bitmap;

import com.jogjanan.kuliner.model.StatusAction;
import com.jogjanan.kuliner.model.User;

public interface OnAccountChangedListener {
	void onAccountSaved(boolean resut);

	void onAccountLoaded(StatusAction success, User user);

	void onPictureLoaded(StatusAction success, Bitmap bp);
}
