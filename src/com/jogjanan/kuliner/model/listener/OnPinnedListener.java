package com.jogjanan.kuliner.model.listener;


public interface OnPinnedListener {

	void onPinned(Boolean status);

}
