package com.jogjanan.kuliner.model.listener;

import com.jogjanan.kuliner.model.StatusAction;

public interface OnGcmRegisteredListener {
	void onGCMRegistered(StatusAction status, String message);
}
