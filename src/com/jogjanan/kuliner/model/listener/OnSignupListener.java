package com.jogjanan.kuliner.model.listener;

import com.jogjanan.kuliner.model.User;

public interface OnSignupListener {

	void onSignup(User user, Boolean status);

}
