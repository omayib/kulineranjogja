package com.jogjanan.kuliner.model.listener;


public interface OnLogoutListener {

	void onLogout(boolean status);

}
