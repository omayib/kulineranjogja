package com.jogjanan.kuliner.model.listener;

import java.util.ArrayList;

import com.jogjanan.kuliner.model.Restaurant;
import com.jogjanan.kuliner.model.StatusAction;

public interface OnNearbyAddedListener {

	void onNearbyAdded(ArrayList<Restaurant> resto, StatusAction status);
}
