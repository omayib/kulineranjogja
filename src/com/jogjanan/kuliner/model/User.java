package com.jogjanan.kuliner.model;

import android.graphics.Bitmap;

public class User {
	private String name;
	private String token;
	private int ID;
	private String email;
	private String password;
	private String photo = null;

	public User(String name, String token, int i, String email,
			String password, String photo) {
		super();
		this.name = name;
		this.token = token;
		ID = i;
		this.email = email;
		this.password = password;
		this.photo = photo;
	}

	public String getPhoto() {
		return photo;
	}

	public String getPassword() {
		return password;
	}

	public String getName() {
		return name;
	}

	public String getToken() {
		return token;
	}

	public int getID() {
		return ID;
	}

	public String getEmail() {
		return email;
	}

}
