package com.jogjanan.kuliner.model;

import java.util.ArrayList;

public class ItemMenuRestoran {
	private String name, photo;
	private double harga;
	private float rate;
	private boolean recommended;
	private int idRestoran, idMenuRestoran;
	private ArrayList<Taste> tastes;
	private String stastes;

	public ItemMenuRestoran(String name, double harga, String photo,
			float rate, boolean recommended, int idRestoran,
			int idMenuRestoran, String tastes) {
		super();
		this.name = name;
		this.harga = harga;
		this.photo = photo;
		this.rate = rate;
		this.recommended = recommended;
		this.idRestoran = idRestoran;
		this.idMenuRestoran = idMenuRestoran;
		this.stastes = tastes;
	}

	public ItemMenuRestoran(String name, double harga, String photo,
			float rate, boolean recommended, int idRestoran,
			int idMenuRestoran, ArrayList<Taste> tastes) {
		super();
		this.name = name;
		this.photo = photo;
		this.harga = harga;
		this.rate = rate;
		this.recommended = recommended;
		this.idRestoran = idRestoran;
		this.idMenuRestoran = idMenuRestoran;
		this.tastes = tastes;
	}

	public String getTasteAsString() {
		return stastes;
	}

	public ArrayList<Taste> getTastes() {
		return tastes;
	}

	public int getIdRestaurant() {
		return idRestoran;
	}

	public int getIdMenuRestaurant() {
		return idMenuRestoran;
	}

	public String getName() {
		return name;
	}

	public double getHarga() {
		return harga;
	}

	public String getPhoto() {
		return photo;
	}

	public float getRate() {
		return rate;
	}

	public boolean isRecommended() {
		return recommended;
	}

}
