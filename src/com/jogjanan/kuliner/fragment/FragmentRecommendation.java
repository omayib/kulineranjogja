package com.jogjanan.kuliner.fragment;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.jogjanan.kuliner.DetailActivity;
import com.jogjanan.kuliner.R;
import com.jogjanan.kuliner.adapter.ListRecommendedItem;
import com.jogjanan.kuliner.application.JogjananApplication;
import com.jogjanan.kuliner.impl.DataGenerator;
import com.jogjanan.kuliner.impl.listenermanager.RecommendationListenerManager;
import com.jogjanan.kuliner.model.ItemRecommendation;
import com.jogjanan.kuliner.model.StatusAction;
import com.jogjanan.kuliner.model.listener.OnRecommendationAddedListener;

public class FragmentRecommendation extends Fragment implements
		OnRecommendationAddedListener {
	private ListView lv;
	private JogjananApplication app;
	private RecommendationListenerManager recommendationListenerManager;
	private ArrayList<ItemRecommendation> dataRecommendation;
	private ListRecommendedItem adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		app = (JogjananApplication) getActivity().getApplication();
		recommendationListenerManager = app.getRecommendationManager();
		recommendationListenerManager.setRecommendationListener(this);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View convertview = inflater.inflate(
				com.jogjanan.kuliner.R.layout.fragment_recommendation,
				container, false);
		lv = (ListView) convertview.findViewById(R.id.listRecommendation);

		return convertview;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		DataGenerator data = app.getDataGenerator();
		data.generateData();
		dataRecommendation = data.getRecommendation();

		adapter = new ListRecommendedItem(app.getApplicationContext(),
				dataRecommendation);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(), DetailActivity.class);
				intent.putExtra("bundle_idLocalResto",
						dataRecommendation.get(arg2).getIdRestoLocal());
				intent.putExtra("bundle_idResto", dataRecommendation.get(arg2)
						.getIdResto());
				getActivity().startActivity(intent);

			}
		});
	}

	@Override
	public void onRecommendationAdded(
			ArrayList<ItemRecommendation> itemRecommendation, StatusAction status) {
		// TODO Auto-generated method stub
		for (ItemRecommendation itemRecommendation2 : itemRecommendation) {
			dataRecommendation.add(itemRecommendation2);
		}
		adapter.notifyDataSetChanged();
	}
}