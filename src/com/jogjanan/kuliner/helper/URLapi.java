package com.jogjanan.kuliner.helper;

public class URLapi {
	private static String base = "http://192.168.56.1";// localhost
	public static String login = base + "/JogjananKuliner/v1/signin";
	public static String signup = base + "/JogjananKuliner/v1/signup";
	public static String logout = base + "/JogjananKuliner/v1/logout";
	public static String updateProfile = base
			+ "/JogjananKuliner/v1/updateProfile";
	public static String getItemPreference = base
			+ "/JogjananKuliner/v1/getItemPreference";
	public static String updatePreference = base
			+ "/JogjananKuliner/v1/updatePreference";
	public static String getAccount = base
			+ "/JogjananKuliner/v1/getUserProfile";

}