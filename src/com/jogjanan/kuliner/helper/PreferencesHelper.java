package com.jogjanan.kuliner.helper;

public class PreferencesHelper {
	public static String PREF_NAME = "JogjananPreference";
	public static String TOKEN = "token";
	public static String USERNAME = "username";
	public static String EXTRA_MESSAGE = "message";
	public static String PROPERTY_REG_ID = "registration_id";
	public static final String PROPERTY_APP_VERSION = "appVersion";
	public static final String NEWUSER = "newUser";
	public static final String PHOTO_PATH = "photo";

}
