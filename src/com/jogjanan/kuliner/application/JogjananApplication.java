package com.jogjanan.kuliner.application;

import java.io.IOException;

import android.app.Application;
import android.content.SharedPreferences;
import android.database.SQLException;

import com.jogjanan.kuliner.helper.PreferencesHelper;
import com.jogjanan.kuliner.impl.DataGenerator;
import com.jogjanan.kuliner.impl.action.PreferenceAction;
import com.jogjanan.kuliner.impl.listenermanager.AccountChangedListenerManager;
import com.jogjanan.kuliner.impl.listenermanager.AuthListenerManager;
import com.jogjanan.kuliner.impl.listenermanager.GCMListenerManager;
import com.jogjanan.kuliner.impl.listenermanager.NearbyListenerManager;
import com.jogjanan.kuliner.impl.listenermanager.PreferenceListenerManager;
import com.jogjanan.kuliner.impl.listenermanager.RecommendationListenerManager;
import com.jogjanan.kuliner.impl.listenermanager.SignupListenerManager;
import com.jogjanan.kuliner.sqlite.DataBaseHelper;

public class JogjananApplication extends Application {

	private DataBaseHelper db;
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor editorPreferences;
	private DataGenerator data;

	private AuthListenerManager listenerManager;
	private SignupListenerManager signupManager;
	private RecommendationListenerManager recManager;
	private NearbyListenerManager nearbyListenerManager;
	private GCMListenerManager registerGcmManager;
	private AccountChangedListenerManager accountManager;
	private PreferenceListenerManager prefereceLoadedManager;
	private PreferenceAction preferenceAction;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		listenerManager = new AuthListenerManager();
		signupManager = new SignupListenerManager();
		recManager = new RecommendationListenerManager();
		nearbyListenerManager = new NearbyListenerManager();
		registerGcmManager = new GCMListenerManager();
		accountManager = new AccountChangedListenerManager();
		prefereceLoadedManager = new PreferenceListenerManager();

		setupdDatabase();

		sharedPreferences = this.getSharedPreferences(
				PreferencesHelper.PREF_NAME, 0);
		editorPreferences = sharedPreferences.edit();
		preferenceAction = new PreferenceAction(sharedPreferences,
				editorPreferences);

		data = new DataGenerator(db);

	}

	public PreferenceListenerManager getPreferenceLoadedManager() {
		return prefereceLoadedManager;
	}

	public AccountChangedListenerManager getAccountListenerManager() {
		return accountManager;

	}

	public PreferenceAction getPreferenceAction() {
		return preferenceAction;
	}

	public GCMListenerManager getGcmListenerManager() {
		return registerGcmManager;
	}

	public NearbyListenerManager getNearbyListenerManager() {
		return nearbyListenerManager;
	}

	public RecommendationListenerManager getRecommendationManager() {
		return recManager;
	}

	public SignupListenerManager getSignupListenerManager() {
		return this.signupManager;
	}

	public AuthListenerManager getAuthListenerManager() {
		return this.listenerManager;
	}

	public DataGenerator getDataGenerator() {
		return data;
	}

	public SharedPreferences getSharedPreferences() {
		return sharedPreferences;
	}

	public SharedPreferences.Editor getEditorPreferences() {
		return editorPreferences;
	}

	public DataBaseHelper getDatabaseHelper() {
		return db;
	}

	private void setupdDatabase() {
		// TODO Auto-generated method stub
		db = new DataBaseHelper(this);
		try {
			db.createDataBase();
		} catch (IOException ioe) {
			throw new Error("Unable to create database");
		}
		try {
			if (!db.isOpenDB())
				db.openDataBase();
		} catch (SQLException sqle) {
			throw sqle;
		}
	}

	public void clearDatabase() {
		db.clearDatabase();
	}

}
