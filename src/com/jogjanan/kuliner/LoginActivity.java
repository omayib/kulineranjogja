package com.jogjanan.kuliner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.jogjanan.kuliner.application.JogjananApplication;
import com.jogjanan.kuliner.impl.action.PreferenceAction;
import com.jogjanan.kuliner.impl.action.UserAuthAction;
import com.jogjanan.kuliner.impl.action.UserRegisterGCMAction;
import com.jogjanan.kuliner.impl.listenermanager.AuthListenerManager;
import com.jogjanan.kuliner.impl.listenermanager.GCMListenerManager;
import com.jogjanan.kuliner.model.StatusAction;
import com.jogjanan.kuliner.model.User;
import com.jogjanan.kuliner.model.listener.OnGcmRegisteredListener;
import com.jogjanan.kuliner.model.listener.OnLoginListener;
import com.jogjanan.kuliner.model.listener.OnSignupListener;

public class LoginActivity extends Activity implements OnLoginListener,
		OnGcmRegisteredListener, OnClickListener, OnSignupListener {

	private JogjananApplication app;
	// about listener manager
	private AuthListenerManager authListenerManager;
	private GCMListenerManager gcmListenerManager;
	// about user action
	private PreferenceAction preferenceAction;
	private UserRegisterGCMAction userRegisterGcmAction;
	private UserAuthAction auth;
	// all about GCM
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private GoogleCloudMessaging gcm;
	private String regid;
	private String SENDER_ID = "87109524894";// iron-handler-491
	private boolean GCM_REGISTERED = false;
	// about widget on layout
	private Button bSignin, bSignup;
	private EditText inEmail, inPassword;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		app = (JogjananApplication) getApplication();
		preferenceAction = app.getPreferenceAction();

		authListenerManager = app.getAuthListenerManager();
		authListenerManager.setOnLoginListener(this);
		authListenerManager.setOnSignupListener(this);
		auth = new UserAuthAction(authListenerManager);

		gcmListenerManager = app.getGcmListenerManager();
		gcmListenerManager.setOnGcmRegisteredListener(this);
		userRegisterGcmAction = new UserRegisterGCMAction(gcmListenerManager);

		inEmail = (EditText) findViewById(R.id.login_username);
		inPassword = (EditText) findViewById(R.id.login_password);

		findViewById(R.id.login_blogin).setOnClickListener(this);
		findViewById(R.id.login_signup).setOnClickListener(this);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		registeringGCM();
		checkToken();
	}

	private void checkToken() {
		// TODO Auto-generated method stub
		if (!preferenceAction.getToken().isEmpty()) {
			preferenceAction.setNewUser(false);
			startActivity(new Intent(this, MainActivity.class));
			finish();
		}
	}

	private void registeringGCM() {
		// TODO Auto-generated method stub
		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(getApplicationContext());
			if (regid.isEmpty()) {
				userRegisterGcmAction.doRegister(this, SENDER_ID);
				GCM_REGISTERED = false;
			} else {
				GCM_REGISTERED = true;
			}
		} else {
			Log.i("omayib", "No valid Google Play Services APK found.");
		}
	}

	@Override
	public void onLogin(User user, Boolean callback) {
		// TODO Auto-generated method stub
		if (callback) {
			preferenceAction.setNewUser(false);
			preferenceAction.setToken(user.getToken());
			checkToken();
		} else {
			Toast.makeText(this, "login failed", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onSignup(User user, Boolean status) {
		// TODO Auto-generated method stub
		if (status) {
			preferenceAction.setToken(user.getToken());
			if (!preferenceAction.getToken().isEmpty()) {
				preferenceAction.setNewUser(true);
				startActivity(new Intent(this, ProfileAccountActivity.class));
				finish();
			}
		} else {
			Toast.makeText(this, "signup failed", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onGCMRegistered(StatusAction status, String message) {
		// TODO Auto-generated method stub
		if (status == StatusAction.SUCCESS) {
			// store GCM registration ID
			GCM_REGISTERED = true;
			System.out.println(message);
			preferenceAction.storeGCMRegistrationId(message,
					getAppVersion(this));
		} else {
			GCM_REGISTERED = false;
		}
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i("omayib", "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	private String getRegistrationId(Context context) {
		String registrationId = preferenceAction.getGCMRegistrationId();
		if (registrationId.isEmpty()) {
			Log.i("12", "Registration not found.");
			return "";
		}
		Log.i("23", registrationId);
		int registeredVersion = preferenceAction.getRegisteredVersion();
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i("omayib", "App version changed.");
			return "";
		}
		Log.i("34", String.valueOf(currentVersion));
		return registrationId;
	}

	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (inEmail.getText().toString().isEmpty()
				|| inPassword.getText().toString().isEmpty()) {
			Toast.makeText(this, "fill email and password", Toast.LENGTH_SHORT)
					.show();
			return;
		}
		switch (arg0.getId()) {
		case R.id.login_blogin:

			if (!GCM_REGISTERED) {
				registeringGCM();
				Toast.makeText(this, "failed registering GCM. Retrying..",
						Toast.LENGTH_SHORT).show();
			} else {
				auth.doLogin(this, inEmail.getText().toString(), inPassword
						.getText().toString(), preferenceAction
						.getGCMRegistrationId());
			}

			break;

		case R.id.login_signup:
			System.out.println("clicked");
			if (!GCM_REGISTERED) {
				registeringGCM();
				Toast.makeText(this, "failed registering GCM. Retrying..",
						Toast.LENGTH_SHORT).show();
			} else {
				auth.doSignup(this, inEmail.getText().toString(), inPassword
						.getText().toString(), preferenceAction
						.getGCMRegistrationId());
			}

			break;
		}
	}

}
