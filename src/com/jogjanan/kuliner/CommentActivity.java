package com.jogjanan.kuliner;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.jogjanan.kuliner.adapter.ListCommentAdapter;
import com.jogjanan.kuliner.application.JogjananApplication;
import com.jogjanan.kuliner.impl.DataGenerator;
import com.jogjanan.kuliner.model.Comment;

public class CommentActivity extends Activity {
	private ListView lv;
	private EditText commentText;
	private ImageButton bSend;
	private JogjananApplication app;
	private DataGenerator data;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		ActionBar ab = getActionBar();
		ab.setHomeButtonEnabled(true);
		ab.setTitle("Comment");
		ab.setSubtitle("Home > Detail Restaurant > comment");

		app = (JogjananApplication) getApplication();

		setContentView(R.layout.fragment_comment);
		lv = (ListView) findViewById(R.id.listComments);
		bSend = (ImageButton) findViewById(R.id.bSubmit);
		commentText = (EditText) findViewById(R.id.commentInput);

		bSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Bundle bundle = getIntent().getExtras();
		if (bundle == null)
			return;
		final int idLocalResto = bundle.getInt("bundle_idLocalResto");
		data = app.getDataGenerator();
		ArrayList<Comment> listComment = data.getCommentResto(idLocalResto);

		ListCommentAdapter adapter = new ListCommentAdapter(this, listComment);
		lv.setAdapter(adapter);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
