package com.jogjanan.kuliner;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.RatingBar;

import com.jogjanan.kuliner.adapter.ListPreferenceAdapter;
import com.jogjanan.kuliner.application.JogjananApplication;
import com.jogjanan.kuliner.impl.action.PreferenceAction;
import com.jogjanan.kuliner.impl.action.UserSettingPreferenceAction;
import com.jogjanan.kuliner.impl.listenermanager.PreferenceListenerManager;
import com.jogjanan.kuliner.model.ItemPreference;
import com.jogjanan.kuliner.model.StatusAction;
import com.jogjanan.kuliner.model.listener.OnPreferenceActionListener;

public class ProfilePreferenceActivity extends Activity implements
		OnPreferenceActionListener {
	private JogjananApplication app;
	private PreferenceListenerManager prefManager;
	private UserSettingPreferenceAction userLoadPrefAction;
	private ListPreferenceAdapter prefAdapter;
	private ListView lv;
	private PreferenceAction prefAction;
	private RatingBar manis, asam, asin, gurih, pahit, pedas, panas, dingin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting_preference);
		app = (JogjananApplication) getApplication();
		prefAction = app.getPreferenceAction();

		ActionBar ab = getActionBar();
		if (prefAction.isNewUser()) {
			ab.setTitle("Setup profile");
			ab.setSubtitle("Setup your preferences");
			ab.setDisplayHomeAsUpEnabled(false);

		} else {
			ab.setTitle("Setting");
			ab.setSubtitle("Setting > preference");
			ab.setDisplayHomeAsUpEnabled(true);

		}
		prefManager = app.getPreferenceLoadedManager();
		prefManager.setOnPreferenceLoadedListener(this);
		userLoadPrefAction = new UserSettingPreferenceAction(prefManager);

		manis = (RatingBar) findViewById(R.id.rattingManis);
		asam = (RatingBar) findViewById(R.id.rattingAsam);
		asin = (RatingBar) findViewById(R.id.rattingAsin);
		pahit = (RatingBar) findViewById(R.id.rattingPahit);
		gurih = (RatingBar) findViewById(R.id.rattingGurih);
		pedas = (RatingBar) findViewById(R.id.rattingPedas);
		panas = (RatingBar) findViewById(R.id.rattingPanas);
		dingin = (RatingBar) findViewById(R.id.rattingDingin);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.account, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.saved:
			ItemPreference pref = new ItemPreference(manis.getRating(),
					asam.getRating(), asin.getRating(), pahit.getRating(),
					gurih.getRating(), pedas.getRating(), panas.getRating(),
					dingin.getRating());
			userLoadPrefAction
					.savePreference(this, prefAction.getToken(), pref);
			break;

		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		userLoadPrefAction.loadPreference(this, prefAction.getToken());
	}

	@Override
	public void onPreferenceLoaded(ItemPreference pref, StatusAction status) {
		// TODO Auto-generated method stub
		if (status == StatusAction.FAILED) {
			// toast
			return;
		}
		manis.setRating(pref.getManis());
		asam.setRating(pref.getAsam());
		asin.setRating(pref.getAsin());
		pahit.setRating(pref.getPahit());
		gurih.setRating(pref.getGurih());
		panas.setRating(pref.getPanas());
		dingin.setRating(pref.getDingin());
		pedas.setRating(pref.getPedas());

	}

	@Override
	public void onPreferenceSaved(StatusAction status) {
		// TODO Auto-generated method stub
		if (status == StatusAction.SUCCESS) {
			System.out.println("saved!");
		}
		if (prefAction.isNewUser()) {
			startActivity(new Intent(this, MainActivity.class));
			finish();
		}
	}
}
