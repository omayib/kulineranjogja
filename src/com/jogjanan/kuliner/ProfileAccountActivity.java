package com.jogjanan.kuliner;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.jogjanan.kuliner.application.JogjananApplication;
import com.jogjanan.kuliner.impl.action.PreferenceAction;
import com.jogjanan.kuliner.impl.action.UserSettingAccountAction;
import com.jogjanan.kuliner.impl.listenermanager.AccountChangedListenerManager;
import com.jogjanan.kuliner.model.StatusAction;
import com.jogjanan.kuliner.model.User;
import com.jogjanan.kuliner.model.listener.OnAccountChangedListener;

public class ProfileAccountActivity extends Activity implements
		OnAccountChangedListener {
	private ImageView profilePic;
	private EditText profileEmail, profileName, ProfilePassword;
	private RadioButton bmale, bfemale;
	private RadioGroup bGender;

	private JogjananApplication app;
	private PreferenceAction preferenceAction;
	private AccountChangedListenerManager accountListenerManager;
	private UserSettingAccountAction userSettingAccountAction;
	private static final int SELECT_PICTURE = 1;
	private String selectedImagePath = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting_account);
		app = (JogjananApplication) getApplication();
		preferenceAction = app.getPreferenceAction();

		ActionBar ab = getActionBar();
		if (preferenceAction.isNewUser()) {
			ab.setTitle("Setup profile");
			ab.setSubtitle("Setup your account");
			ab.setDisplayHomeAsUpEnabled(false);

		} else {
			ab.setTitle("Setting");
			ab.setSubtitle("Setting > account");
			ab.setDisplayHomeAsUpEnabled(true);

		}

		accountListenerManager = app.getAccountListenerManager();
		accountListenerManager.setAccountSavedListener(this);

		userSettingAccountAction = new UserSettingAccountAction(
				accountListenerManager);

		profileEmail = (EditText) findViewById(R.id.profilEmail);
		ProfilePassword = (EditText) findViewById(R.id.profilPassword);
		profileName = (EditText) findViewById(R.id.profilName);
		profilePic = (ImageView) findViewById(R.id.profilpic);
		profilePic.setImageDrawable(getResources().getDrawable(
				R.drawable.user_boy));
		bGender = (RadioGroup) findViewById(R.id.profilGander);
		bmale = (RadioButton) findViewById(R.id.male);
		bfemale = (RadioButton) findViewById(R.id.female);

		profilePic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(
						Intent.createChooser(intent, "Select Picture"),
						SELECT_PICTURE);
			}
		});

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		userSettingAccountAction.getAccountFromApi(this,
				preferenceAction.getToken());
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				Uri selectedImageUri = data.getData();
				selectedImagePath = getPath(selectedImageUri);
				System.out.println(selectedImagePath);

				Bitmap yourSelectedImage = BitmapFactory
						.decodeFile(selectedImagePath);
				profilePic.setImageBitmap(yourSelectedImage);
			}
		}
	}

	private String getPath(Uri uri) {

		if (uri == null) {
			return null;
		}

		// this will only work for images selected from gallery
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		if (cursor != null) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}

		return uri.getPath();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.account, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.saved:
			User u = new User(profileName.getText().toString(),
					preferenceAction.getToken(), 0, profileEmail.getText()
							.toString(), ProfilePassword.getText().toString(),
					"a", "a");
			userSettingAccountAction.doSave(this, u);
			break;

		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	public void onAccountSaved(boolean saved) {
		// TODO Auto-generated method stub
		if (!saved) {
			Toast.makeText(this, "failed to saved", Toast.LENGTH_SHORT).show();
		} else {
			preferenceAction.setPhotoPath(selectedImagePath);
		}
		if (preferenceAction.isNewUser()) {
			startActivity(new Intent(this, ProfilePreferenceActivity.class));
			finish();
			return;
		}

	}

	@Override
	public void onAccountLoaded(StatusAction status, User user) {
		// TODO Auto-generated method stub
		if (status == StatusAction.SUCCESS) {
			System.out.println(user.getEmail());
			System.out.println(user.getPassword());
			System.out.println(user.getName());
			System.out.println(user.getID());

			profileEmail.setText(user.getEmail());
			profileName.setText(user.getName());
			ProfilePassword.setText(user.getPassword());

			if (!user.getPhoto().isEmpty()) {
				userSettingAccountAction.loadPicture(user.getPhoto());
			}

		}
	}

	@Override
	public void onPictureLoaded(StatusAction success, Bitmap bp) {
		// TODO Auto-generated method stub
		if (StatusAction.SUCCESS == success) {
			profilePic.setImageBitmap(bp);
		}
	}

}
