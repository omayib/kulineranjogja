package com.jogjanan.kuliner;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Toast;

import com.drawer.nav.AbstractNavDrawerActivity;
import com.drawer.nav.NavDrawerActivityConfiguration;
import com.drawer.nav.NavDrawerAdapter;
import com.drawer.nav.NavDrawerItem;
import com.drawer.nav.NavMenuItem;
import com.drawer.nav.NavMenuSection;
import com.jogjanan.kuliner.application.JogjananApplication;
import com.jogjanan.kuliner.fragment.FragmentRecommendation;
import com.jogjanan.kuliner.impl.action.PreferenceAction;
import com.jogjanan.kuliner.impl.action.UserAuthAction;
import com.jogjanan.kuliner.impl.listenermanager.AuthListenerManager;
import com.jogjanan.kuliner.model.listener.OnLogoutListener;

public class MainActivity extends AbstractNavDrawerActivity implements
		OnLogoutListener {

	private JogjananApplication app;
	private PreferenceAction preferenceAction;
	private AuthListenerManager authListenerManager;
	private UserAuthAction authAction;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		app = (JogjananApplication) getApplication();
		preferenceAction = app.getPreferenceAction();

		authListenerManager = app.getAuthListenerManager();
		authListenerManager.setLogoutListener(this);
		authAction = new UserAuthAction(authListenerManager);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, new FragmentRecommendation())
					.commit();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected NavDrawerActivityConfiguration getNavDrawerConfiguration() {
		// TODO Auto-generated method stub
		NavDrawerItem[] menu = new NavDrawerItem[] {
				NavMenuSection.create(100, "Main Menu"),
				NavMenuItem.create(101, "Recommended", "navdrawer_friends",
						false, this),
				NavMenuItem.create(102, "Pinned", "navdrawer_airport", true,
						this),
				NavMenuItem.create(103, "Nearby", "navdrawer_airport", true,
						this),
				NavMenuItem.create(104, "Setting", "navdrawer_airport", true,
						this),
				NavMenuItem.create(105, "Logout", "navdrawer_airport", true,
						this) };

		NavDrawerActivityConfiguration navDrawerActivityConfiguration = new NavDrawerActivityConfiguration();
		navDrawerActivityConfiguration.setMainLayout(R.layout.activity_main);
		navDrawerActivityConfiguration.setDrawerLayoutId(R.id.drawer_layout);
		navDrawerActivityConfiguration.setLeftDrawerId(R.id.left_drawer);
		navDrawerActivityConfiguration.setNavItems(menu);
		navDrawerActivityConfiguration
				.setDrawerShadow(R.drawable.drawer_shadow);
		navDrawerActivityConfiguration.setDrawerOpenDesc(R.string.drawer_open);
		navDrawerActivityConfiguration
				.setDrawerCloseDesc(R.string.drawer_close);
		navDrawerActivityConfiguration.setBaseAdapter(new NavDrawerAdapter(
				this, R.layout.navdrawer_item, menu));
		return navDrawerActivityConfiguration;
	}

	@Override
	protected void onNavItemSelected(int id) {
		// TODO Auto-generated method stub
		switch ((int) id) {

		case 104:
			startActivity(new Intent(this, SettingActivity.class));

			break;
		case 105:
			authAction.doLogout(this, preferenceAction.getToken());
			break;
		}
	}

	@Override
	public void onLogout(boolean status) {
		// TODO Auto-generated method stub
		if (status) {
			app.clearDatabase();
			preferenceAction.clearPreferences();
			finish();
		} else {
			Toast.makeText(this, "failed logout", Toast.LENGTH_SHORT).show();
		}
	}

}
