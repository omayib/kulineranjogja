package com.jogjanan.kuliner.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import com.jogjanan.kuliner.R;
import com.jogjanan.kuliner.model.ItemPreference;

public class ListPreferenceAdapter extends BaseAdapter {
	private ArrayList<ItemPreference> listRecommendation;

	private LayoutInflater mInflater;

	public ListPreferenceAdapter(Context context, ArrayList<ItemPreference> res) {
		listRecommendation = res;
		mInflater = LayoutInflater.from(context);
	}

	public int getCount() {
		return listRecommendation.size();
	}

	public Object getItem(int position) {
		return listRecommendation.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_preference, null);
			holder = new ViewHolder();
			holder.taste = (TextView) convertView.findViewById(R.id.taste);
			holder.rating = (RatingBar) convertView
					.findViewById(R.id.rattingAsam);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// holder.taste.setText(listRecommendation.get(position).getTaste());
		// holder.rating.setRating(listRecommendation.get(position).getLevel());
		// holder.rating
		// .setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
		//
		// @Override
		// public void onRatingChanged(RatingBar ratingBar,
		// float rating, boolean fromUser) {
		// // TODO Auto-generated method stub
		//
		// }
		// });
		return convertView;
	}

	static class ViewHolder {
		TextView taste;
		RatingBar rating;
	}
}