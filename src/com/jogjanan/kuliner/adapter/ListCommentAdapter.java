package com.jogjanan.kuliner.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jogjanan.kuliner.R;
import com.jogjanan.kuliner.model.Comment;

public class ListCommentAdapter extends BaseAdapter {
	private ArrayList<Comment> listComment;

	private LayoutInflater mInflater;

	public ListCommentAdapter(Context context, ArrayList<Comment> res) {
		listComment = res;
		mInflater = LayoutInflater.from(context);
	}

	public int getCount() {
		return listComment.size();
	}

	public Object getItem(int position) {
		return listComment.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_comment, null);
			holder = new ViewHolder();
			holder.message = (TextView) convertView
					.findViewById(R.id.commentName);
			holder.date = (TextView) convertView
					.findViewById(R.id.commentDate);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.message.setText(listComment.get(position).getName());
		holder.date.setText(listComment.get(position).getDate());

		return convertView;
	}

	static class ViewHolder {
		TextView message, date;
		ImageView pic;
	}
}