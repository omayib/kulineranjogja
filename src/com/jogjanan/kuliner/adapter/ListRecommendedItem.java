package com.jogjanan.kuliner.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.jogjanan.kuliner.R;
import com.jogjanan.kuliner.model.ItemRecommendation;
import com.jogjanan.kuliner.model.Restaurant;

public class ListRecommendedItem extends BaseAdapter {
	private ArrayList<ItemRecommendation> listRecommendation;

	private LayoutInflater mInflater;

	public ListRecommendedItem(Context context,
			ArrayList<ItemRecommendation> res) {
		listRecommendation = res;
		mInflater = LayoutInflater.from(context);
	}

	public int getCount() {
		return listRecommendation.size();
	}

	public Object getItem(int position) {
		return listRecommendation.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_recommendation, null);
			holder = new ViewHolder();
			holder.nameFood = (TextView) convertView
					.findViewById(R.id.nameFood);
			holder.addressResto = (TextView) convertView
					.findViewById(R.id.addrResto);
			holder.nameResto = (TextView) convertView
					.findViewById(R.id.nameResto);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.nameFood.setText(listRecommendation.get(position).getNameFood());
		holder.addressResto.setText(listRecommendation.get(position)
				.getAddressResto());
		holder.nameResto.setText(listRecommendation.get(position)
				.getNameResto());

		return convertView;
	}

	static class ViewHolder {
		TextView nameFood, nameResto, addressResto;
	}
}