package com.jogjanan.kuliner.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jogjanan.kuliner.R;
import com.jogjanan.kuliner.model.ItemMenuRestoran;

public class ListMenuAdapter extends BaseAdapter {
	private ArrayList<ItemMenuRestoran> listRecommendation;

	private LayoutInflater mInflater;

	public ListMenuAdapter(Context context, ArrayList<ItemMenuRestoran> res) {
		listRecommendation = res;
		mInflater = LayoutInflater.from(context);
	}

	public int getCount() {
		return listRecommendation.size();
	}

	public Object getItem(int position) {
		return listRecommendation.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_menu_resto, null);
			holder = new ViewHolder();
			holder.nameFood = (TextView) convertView
					.findViewById(R.id.menu_name);
			holder.totalComment = (TextView) convertView
					.findViewById(R.id.menu_commentTotal);
			holder.rating = (RatingBar) convertView
					.findViewById(R.id.menu_ratting);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.nameFood.setText(listRecommendation.get(position).getName());
		holder.totalComment.setText(String.valueOf(listRecommendation.get(
				position).getHarga()));
		holder.rating.setRating(listRecommendation.get(position).getRate());

		return convertView;
	}

	static class ViewHolder {
		TextView nameFood, totalComment;
		ImageView pic;
		RatingBar rating;
	}
}