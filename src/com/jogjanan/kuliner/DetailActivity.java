package com.jogjanan.kuliner;

import java.util.ArrayList;

import android.app.ActionBar;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.jogjanan.kuliner.adapter.ListMenuAdapter;
import com.jogjanan.kuliner.application.JogjananApplication;
import com.jogjanan.kuliner.impl.DataGenerator;
import com.jogjanan.kuliner.model.ItemMenuRestoran;
import com.jogjanan.kuliner.model.Restaurant;

public class DetailActivity extends FragmentActivity {
	private ListView lv;
	private ImageView imResto;
	private TextView address, name;
	private Button bComment;
	private ToggleButton bPin;
	private ImageButton bMap;

	private JogjananApplication app;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		ActionBar ab = getActionBar();
		ab.setHomeButtonEnabled(true);
		ab.setTitle("Detail Restaurant");
		ab.setSubtitle("Home > Detail Restaurant");

		setContentView(R.layout.fragment_detail_resto);

		app = (JogjananApplication) getApplication();

		lv = (ListView) findViewById(R.id.listMenu);
		imResto = (ImageView) findViewById(R.id.imgViewResto);
		address = (TextView) findViewById(R.id.detail_addressResto);
		name = (TextView) findViewById(R.id.menu_commentTotal);
		bComment = (Button) findViewById(R.id.bComment);
		bPin = (ToggleButton) findViewById(R.id.bPin);
		bMap = (ImageButton) findViewById(R.id.bMap);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Bundle bundle = getIntent().getExtras();
		if (bundle == null)
			return;
		final int idLocalResto = bundle.getInt("bundle_idLocalResto");
		int idResto = bundle.getInt("bundle_idResto");

		DataGenerator data = app.getDataGenerator();
		final Restaurant dataResto = data.getDetailResto(idLocalResto);

		ArrayList<ItemMenuRestoran> menus = data.getMenuRestaurant(idResto);

		ListMenuAdapter menuAdapter = new ListMenuAdapter(this, menus);
		lv.setAdapter(menuAdapter);

		name.setText(dataResto.getName());
		bComment.setText(dataResto.getCommentTotal() + " comment");
		address.setText(dataResto.getAddress());

		bComment.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(DetailActivity.this,
						CommentActivity.class);
				intent.putExtra("bundle_idLocalResto", idLocalResto);
				startActivity(intent);
			}
		});

		bMap.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String uri = "geo:" + String.valueOf(dataResto.getLatitude())
						+ "," + String.valueOf(dataResto.getLongitude());
				startActivity(new Intent(android.content.Intent.ACTION_VIEW,
						Uri.parse(uri)));
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
