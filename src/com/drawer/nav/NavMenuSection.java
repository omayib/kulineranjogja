package com.drawer.nav;

public class NavMenuSection implements NavDrawerItem {

	static final int SECTION_TYPE = 0;
	int id;
	String label;

	public NavMenuSection() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static NavMenuSection create(int id, String label) {
		NavMenuSection section = new NavMenuSection();
		section.setLabel(label);
		return section;

	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return id;
	}

	public void setID(int _id) {
		this.id = _id;
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return label;
	}

	public void setLabel(String lb) {
		this.label = lb;
	}

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return SECTION_TYPE;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateActionBarTittle() {
		// TODO Auto-generated method stub
		return false;
	}

}
