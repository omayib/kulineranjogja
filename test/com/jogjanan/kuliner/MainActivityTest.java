package com.jogjanan.kuliner;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;

import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;

import static org.robolectric.Robolectric.clickOn;
import static org.robolectric.Robolectric.shadowOf;
import android.content.Intent;
import android.widget.Button;

@RunWith(RobolectricTestRunner.class)
public class MainActivityTest {
	private MainActivity activity;
	private Button button1, button2, button3;

	@Before
	public void setup() {
		this.activity = Robolectric.buildActivity(MainActivity.class).create()
				.get();
		button1 = (Button) activity.findViewById(R.id.myButton1);
		button2 = (Button) activity.findViewById(R.id.myButton2);
		button3 = (Button) activity.findViewById(R.id.myButton3);
	}

	@Test
	public void shouldHaveHappySmile() throws Exception {
		String hello = this.activity.getString(R.string.hello_world);
		assertThat(hello, equalTo("Hello world!"));
	}

	@Test
	public void shouldButton1Clicked() throws Exception {
		assertThat((String) button1.getText(), equalTo("satu"));
	}

	@Test
	public void shouldButton2Clicked() throws Exception {
		assertThat((String) button2.getText(), equalTo("dua"));
	}

	@Test
	public void shouldButton3Clicked() throws Exception {
		assertThat((String) button3.getText(), equalTo("tiga"));
	}

	@Test
	public void onPressButton1() throws Exception {
		button1.performClick();

		ShadowActivity showActivity = shadowOf(activity);
		Intent startedIntent = showActivity.getNextStartedActivity();
		ShadowIntent showIntent = shadowOf(startedIntent);

		assertThat(showIntent.getComponent().getClassName(),
				equalTo(SecondActivity.class.getName()));
	}
}
